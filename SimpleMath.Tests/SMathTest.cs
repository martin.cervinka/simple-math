using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleMath.Tests
{
    [TestClass]
    public class SMathTest
    {
        [TestMethod]
        public void Add()
        {
            // arrange
            int a = 1;
            int b = 2;

            // act
            var c = SMath.Add(a, b);

            // assert
            Assert.AreEqual(c, a + b);
        }

        [TestMethod]
        public void Subtract()
        {
            // arrange
            int a = 1;
            int b = 2;

            // act
            var c = SMath.Subtract(a, b);

            // assert
            Assert.AreEqual(c, a - b);
        }
    }
}